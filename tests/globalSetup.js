const path = require('path');

module.exports = () => {
  process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', 'config');
};
