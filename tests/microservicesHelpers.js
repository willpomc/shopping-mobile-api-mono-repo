const fs = require('fs');

const microservicesDirPath = 'microservices/';
const getMicroserviceNames = () => fs.readdirSync(microservicesDirPath);

module.exports = {
  microservicesDirPath,
  getMicroserviceNames
};
