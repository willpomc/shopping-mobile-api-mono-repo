const getLogger = () => ({
  info: msg => console.info(msg),
  warn: msg => console.warn(msg),
  error: msg => console.error(msg)
});

module.exports = {
  getLogger
};
