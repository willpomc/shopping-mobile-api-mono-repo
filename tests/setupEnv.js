const path = require('path');
const {getMicroserviceNames, microservicesDirPath} = require('./microservicesHelpers');

// Set up NODE_CONFIG_DIR so that integration tests can find the config dir for each
// microservice
const nodeConfigDirValue = getMicroserviceNames().map(microservice => path.join(
  __dirname,
  '..',
  microservicesDirPath,
  microservice,
  '/config'
));

process.env.NODE_CONFIG_DIR = nodeConfigDirValue.join(path.delimiter);
