const jestConfigBase = require('./tests/jest.config.base');
const {getMicroserviceNames} = require('./tests/microservicesHelpers');

const microservicesToAlias = getMicroserviceNames().join('|');

module.exports = {
  ...jestConfigBase,

  testRegex: '((\\.|/)(test))\\.[t]s$',
  testPathIgnorePatterns: [
    '/node_modules/',
    '/tests/'
  ],

  // A map from regular expressions to module names that allow to stub out resources with a single module
  // moduleNameMapper: {},
  moduleNameMapper: {
    '^config$': '<rootDir>/tests/mocks/config.js',
    'shared\/src\/utils\/logger': '<rootDir>/tests/mocks/logger.js',

    // Need this to correctly map alias paths correctly
    // IMPORTANT: make sure this mapping goes last as it acts as a catch all as well.
    // TODO: for some reason ts-jest is not finding the babelConfig (can fix later)
    [`.+(${microservicesToAlias})/src/(.*)`]: '<rootDir>/microservices/$1/src/$2',
  },
};
