#!/bin/sh

# This is the entry point to the docker container. You specify which microservice you want
# to build as the input. Eg: recruitment

INPUT="$1"

if [ "$INPUT" = "" ]
then
  echo "No microservice input provided. Exiting"
  exit 1
fi

yarn build @woolies/$INPUT
yarn start @woolies/$INPUT
