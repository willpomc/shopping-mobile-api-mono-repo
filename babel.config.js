const path = require('path');

module.exports = api => ({
  presets: [
    [
      '@babel/preset-env',
      {
        "targets": {
          "node": "current"
        }
      }
    ],
    '@babel/typescript'
  ],
  plugins: [
  '@babel/proposal-class-properties',
  [
      'module-resolver',
      // {
      //   resolvePath(sourcePath) {
      //     switch (sourcePath) {
      //       case "shared":
      //         return path.resolve(__dirname, "microservices/shared/src");
      //       default:
      //         console.dd(sourcePath);
      //     }
      //   }
      // }
      {
        root: ['./'],
        alias: {
          // shared: './microservices/shared/src'
          // consts: './src/consts',
          // graphql: './src/graphql',
          // utils: './src/utils',
          // helpers: './src/helpers',
          '@woolies/recruitment': '../recruitment/src',
          '@woolies/shared': api.env('test') ? '../shared/src' : '../shared/lib',
          // [camelCase(packageJson.name)]: `../${camelCase(packageJson.name)}/src`,
        },
      },
    ],
  ],
  sourceMaps: 'both',
  babelrcRoots: [
    ".",
    "microservices/*",
  ],
});
