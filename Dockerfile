FROM node:10-alpine

LABEL microservice=@woolies/recruitment

# Copy root package.json and yarn.lock
COPY package.json yarn.lock /shopping-mobile-api-mono-repo/
RUN cd /shopping-mobile-api-mono-repo && yarn

COPY . /shopping-mobile-api-mono-repo

WORKDIR /shopping-mobile-api-mono-repo/

EXPOSE 3000

ENTRYPOINT ["/shopping-mobile-api-mono-repo/.cicd/dockerEntry.sh"]
