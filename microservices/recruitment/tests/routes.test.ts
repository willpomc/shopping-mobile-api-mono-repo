import { getApp } from '@woolies/shared/express/app';
import { getUseCaseRoutes } from '@woolies/recruitment/useCases';
import { RequestListener } from 'http';
import request from 'supertest';
import { destroyMongoDb } from '@woolies/shared/express/model/mongo';

describe('routes', () => {
  let app: RequestListener;
  beforeEach(async () => {
    app = await getApp(getUseCaseRoutes);
  });

  afterEach(async () => {
    await destroyMongoDb();
  });

  it('gets the test user successfully', async () => {
    const response = await request(app).get('/api/v1/user');

    expect(response.status).toBe(200);
    expect(response.body).toMatchSnapshot({
      name: 'Will Po',
      token: expect.any(String), // Do this otherwise token gets committed to repo.
    });
  });

  it(`gets the product list sorted by 'Low' successfully`, async () => {
    const response = await request(app).get('/api/v1/sort?sortOption=Low');

    expect(response.status).toBe(200);
    expect(response.body).toMatchSnapshot();
  });

  it(`gets the product list sorted by 'High' successfully`, async () => {
    const response = await request(app).get('/api/v1/sort?sortOption=High');

    expect(response.status).toBe(200);
    expect(response.body).toMatchSnapshot();
  });

  it(`gets the product list sorted by 'Ascending' successfully`, async () => {
    const response = await request(app).get('/api/v1/sort?sortOption=Ascending');

    expect(response.status).toBe(200);
    expect(response.body).toMatchSnapshot();
  });

  it(`gets the product list sorted by 'Descending' successfully`, async () => {
    const response = await request(app).get('/api/v1/sort?sortOption=Descending');

    expect(response.status).toBe(200);
    expect(response.body).toMatchSnapshot();
  });

  it(`gets the product list sorted by 'Recommended' successfully`, async () => {
    const response = await request(app).get('/api/v1/sort?sortOption=Recommended');

    expect(response.status).toBe(200);
    expect(response.body).toMatchSnapshot();
  });
});
