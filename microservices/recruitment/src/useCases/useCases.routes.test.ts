import { getUseCaseRoutes } from '@woolies/recruitment/useCases/useCases.routes';

jest.mock('@woolies/recruitment/useCases/getTestUserUseCase', () => ({
  getTestUserUseCaseRoute: jest.fn().mockImplementation(() => (['getTestUserUseCase'])),
}));
jest.mock('@woolies/recruitment/useCases/getSortedAvailableProductsUseCase', () => ({
  getSortedAvailableProductsUseCaseRoute: jest.fn().mockImplementation(() => (['getSortedAvailableProductsUseCase'])),
}));
jest.mock('@woolies/recruitment/useCases/getTrolleyTotalUseCase', () => ({
  getTrolleyTotalUseCaseRoute: jest.fn().mockImplementation(() => (['getTrolleyTotalUseCase'])),
}));

describe('useCaseRoutes', () => {
  const mongoDb: any = {};

  it('returns a list of Route objects', () => {
    const result = getUseCaseRoutes(mongoDb);

    expect(result.length).toBeGreaterThan(0);
    expect(result).toMatchSnapshot();
  });
});
