import { Db } from 'mongodb';
import {getTestUserUseCaseRoute} from '@woolies/recruitment/useCases/getTestUserUseCase';
import {getSortedAvailableProductsUseCaseRoute} from '@woolies/recruitment/useCases/getSortedAvailableProductsUseCase';
import {getTrolleyTotalUseCaseRoute} from '@woolies/recruitment/useCases/getTrolleyTotalUseCase';

// Route handlers execute the useCases
export const getUseCaseRoutes = (mongoDb: Db): Route[] => {
  return [
    ...getTestUserUseCaseRoute(mongoDb),
    ...getSortedAvailableProductsUseCaseRoute(),
    ...getTrolleyTotalUseCaseRoute()
  ]
};
