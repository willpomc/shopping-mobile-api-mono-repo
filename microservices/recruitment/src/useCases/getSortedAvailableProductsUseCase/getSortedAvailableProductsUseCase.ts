import {UseCase} from '@woolies/shared/domain/useCase/UseCase';
import {ProductAvailabilityService} from '../../services/productAvailabilityService/productAvailabilityService';
import {
  ProductListDTO
} from '../../services/productAvailabilityService/productList/productList';
import {ProductListMapper} from '../../services/productAvailabilityService/productList/productList.mapper';

interface GetSortedProductsUseCaseProps {
  sortOption: 'Low' | 'High' | 'Ascending' | 'Descending' | 'Recommended'
}

export class GetSortedAvailableProductsUseCase implements UseCase<GetSortedProductsUseCaseProps,ProductListDTO | null> {
  private readonly productAvailabilityService: ProductAvailabilityService;

  constructor(productAvailabilityService: ProductAvailabilityService) {
    this.productAvailabilityService = productAvailabilityService;
  }

  public async execute(request: GetSortedProductsUseCaseProps): Promise<ProductListDTO | null> {
    const {sortOption} = request;
    const sortedAvailableProducts = await this.productAvailabilityService.execute({sortOption});

    return ProductListMapper.toDTO(sortedAvailableProducts);
  }

}
