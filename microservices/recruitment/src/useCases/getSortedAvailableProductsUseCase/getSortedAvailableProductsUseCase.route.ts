import {ProductAvailabilityService} from '@woolies/recruitment/services/productAvailabilityService/productAvailabilityService';
import {GetSortedAvailableProductsUseCase} from '@woolies/recruitment/useCases/getSortedAvailableProductsUseCase/getSortedAvailableProductsUseCase';
import {asyncMiddleware} from '@woolies/shared/express/middleware/asyncMiddleware';
import {Request, Response} from 'express';

export const getSortedAvailableProductsUseCaseRoute = (): Route[] => {
  // Instantiate required Application Service
  const productAvailabilityService = new ProductAvailabilityService();
  const getSortedAvailableProductsUseCase = new GetSortedAvailableProductsUseCase(productAvailabilityService);

  return [{
    path: '/v1/sort',
    method: 'get',
    handler: asyncMiddleware(async (req: Request, res: Response) => {
      const {query: {sortOption}} = req;
      const sortedProducts = await getSortedAvailableProductsUseCase.execute({sortOption});
      res.status(200).send(sortedProducts);
    })
  }];
};
