import {UserRepo} from '@woolies/recruitment/domains/user/user.repo';
import {UserDTO} from '@woolies/recruitment/domains/user/user';
import {UserMapper} from '@woolies/recruitment/domains/user/user.mapper';
import {UseCase} from '@woolies/shared/domain/useCase/UseCase';

export class GetTestUserUseCase implements UseCase<{},UserDTO | null> {
  private readonly userRepo: UserRepo;

  constructor(userRepo: UserRepo) {
    this.userRepo = userRepo;
  }

  public async execute(): Promise<UserDTO | null> {
    const user = await this.userRepo.findTestUser();

    if (!user) {
      return null;
    }

    return UserMapper.toDTO(user);
  }

}
