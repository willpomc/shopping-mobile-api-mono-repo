import {getTestUserUseCaseRoute} from '@woolies/recruitment/useCases/getTestUserUseCase/getTestUserUseCase.route';

jest.mock('@woolies/recruitment/useCases/getTestUserUseCase/getTestUserUseCase', () => ({
  GetTestUserUseCase: class GetTestUserUseCase {
    execute() {
      return {
        name: 'test',
      };
    }
  },
}));

describe('/user route', () => {
  it('sets status 200 and sends the user', async () => {
    const mongoDb: any = {};
    const [testUserRoute] = getTestUserUseCaseRoute(mongoDb);
    const { handler } = testUserRoute;
    const sendMock = jest.fn();
    const statusMock = jest.fn().mockImplementation(() => ({ send: sendMock }));
    const reqMock: any = {};
    const resMock: any = {
      status: statusMock,
    };

    await (handler as Handler)(reqMock, resMock, () => {});

    expect(statusMock).toBeCalledWith(200);
    expect(sendMock).toBeCalledWith({ name: 'test' });
  });
});
