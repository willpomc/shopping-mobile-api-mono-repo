// Repos is the infrastructure layer - only repo interact with the underlying
// store technology (eg. DBs like Mongo or event streaming like Kafka)
import {UserRepo} from '@woolies/recruitment/domains/user/user.repo';

// GetTestUserUseCase represent what this microservice can do for consumers.
// They depend on 'Domains' and 'Repos'.
import {GetTestUserUseCase} from '@woolies/recruitment/useCases/getTestUserUseCase/getTestUserUseCase';

import {asyncMiddleware} from '@woolies/shared/express/middleware/asyncMiddleware';
import {Request, Response} from 'express';
import {Db} from 'mongodb';

export const getTestUserUseCaseRoute = (mongoDb: Db): Route[] => {
  // Instantiate all Repos
  const userRepo = new UserRepo(mongoDb);

  const getUserTestCase = new GetTestUserUseCase(userRepo /* ,someOtherRepos can go here */);

  // We return an array here because we could add v2 routes if we wanted
  // to version via the path.
  return [{
    path: '/v1/user',
    method: 'get',
    handler: asyncMiddleware(async (req: Request, res: Response) => {
      const user = await getUserTestCase.execute();
      res.status(200).send(user);

      // Note: if our requirements get more complicated and we need to
      // have additional logic for each of our handlers (eg. more error
      // or success scenarios that require different messages to be relayed
      // back to the consumer, then I may consider adding an 'extra' layer
      // like 'Controllers' to abstract away that logic to keep this routes
      // layer clean and focused.
      //
      // Or, we could have 1 route file per use case, and have the additional logic
      // embedded in the route file, then have a routes.ts
      // that just aggregates all the individual use case routes. However, adding
      // a controller layer/class would have the added benefit of standardising the
      // logic via an interface/abstract class.
      //
      // But as it stands, 1 or 2 lines of logic per middleware handler
      // is OK for now...
    }),
  }]
};
