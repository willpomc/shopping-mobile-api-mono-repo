import { CalculateOrderTotalService } from '@woolies/recruitment/services/calculateOrderTotalService/calculateOrderTotalService';
import { UseCase } from '@woolies/shared/domain/useCase/UseCase';

export interface TotalRequest {
  Products: {
    Name: string;
    Price: number;
  }[];
  Quantities: {
    Name: string;
    Quantity: number;
  }[];
  Specials: {
    Quantities: {
      Name: string;
      Quantity: number;
    }[];
    Total: number;
  }[];
}

export class GetTrolleyTotalUseCase implements UseCase<TotalRequest, string | null> {
  private readonly calculateOrderTotalService: CalculateOrderTotalService;

  constructor() {
    this.calculateOrderTotalService = new CalculateOrderTotalService({});
  }

  public execute(request: TotalRequest): string {
    return this.calculateOrderTotalService.execute(request);
  }
}
