import {getTrolleyTotalUseCaseRoute} from '@woolies/recruitment/useCases/getTrolleyTotalUseCase/getTrolleyTotalUseCase.route';

jest.mock('@woolies/recruitment/useCases/getTrolleyTotalUseCase/getTrolleyTotalUseCase', () => ({
  GetTrolleyTotalUseCase: class GetTrolleyTotalUseCase {
    execute() {
      return 50;
    }
  },
}));

describe('/trolleyTotal route', () => {
  it('sets status 200 and sends the total', async () => {
    const [testUserRoute] = getTrolleyTotalUseCaseRoute();
    const { handler } = testUserRoute;
    const jsonMock = jest.fn();
    const statusMock = jest.fn().mockImplementation(() => ({ json: jsonMock }));
    const reqMock: any = {};
    const resMock: any = {
      status: statusMock,
    };

    await (handler as Handler)(reqMock, resMock, () => {});

    expect(statusMock).toBeCalledWith(200);
    expect(jsonMock).toBeCalledWith(50);
  });
});
