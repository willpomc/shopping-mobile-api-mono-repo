import {GetTrolleyTotalUseCase} from '@woolies/recruitment/useCases/getTrolleyTotalUseCase/getTrolleyTotalUseCase';
import {asyncMiddleware} from '@woolies/shared/express/middleware/asyncMiddleware';
import {Request, Response} from 'express';
import {getLogger} from '@woolies/shared/utils/logger';

const logger = getLogger('recruitment', module);

export const getTrolleyTotalUseCaseRoute = (): Route[] => {
  const getTrolleyTotalUseCase = new GetTrolleyTotalUseCase();

  return [{
    path: '/v1/trolleyTotal',
    method: 'post',
    handler: asyncMiddleware(async (req: Request, res: Response) => {
      const {body} = req;
      logger.info('Request body for trolley total', {body: JSON.stringify(body)});
      const total = await getTrolleyTotalUseCase.execute(body);
      logger.info(total);
      res.status(200).json(total);
    })
  }];
};
