import { Mapper } from '@woolies/shared/domain/data/Mapper';
import { Product, ProductDTO, ProductRaw } from '@woolies/recruitment/domains/product/product';
import PreciseNumber from 'big.js';

export class ProductMapper extends Mapper {
  public static toDomain(productRaw: ProductRaw): Product {
    const { name, price, quantity } = productRaw;

    return Product.create({
      name,
      price: PreciseNumber(price),
      quantity: PreciseNumber(quantity),
    });
  }

  // Note: we filter out the popularityIndex of a Product
  public static toDTO(product: Product): ProductDTO {
    const { name, price, quantity } = product.props;
    return {
      name,
      price: price.toString(), // to string because we want to capture maximum accuracy of preciseNumber
      quantity: parseFloat(quantity.toString()),
    };
  }
}
