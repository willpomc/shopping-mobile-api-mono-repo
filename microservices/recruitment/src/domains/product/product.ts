import { ValueObject } from '@woolies/shared/domain/valueObject/ValueObject';
import PreciseNumber from 'big.js';

export interface ProductProps {
  name: string;
  price: PreciseNumber;
  quantity: PreciseNumber;
  popularityIndex?: number;
}

export interface ProductRaw {
  name: string;
  price: number;
  quantity: number;
  popularityIndex?: number;
}

export interface ProductDTO {
  name: string;
  price: string;
  quantity: number;
  popularityIndex?: number;
}

export class Product extends ValueObject<ProductProps> {
  constructor(props: ProductProps) {
    super(props);
  }

  // Use a factory method to create Product.
  // Why?
  // Later, it would be good to return something other than Product,
  // maybe a `Result` object (Result<Product>)that encapsulates some error/success result logic
  // so that the consumer does not need to know how to determine an error state
  // for this value object, and associated problems due to inconsistencies.
  public static create(props: ProductProps): Product {
    return new Product(props);
  }
}
