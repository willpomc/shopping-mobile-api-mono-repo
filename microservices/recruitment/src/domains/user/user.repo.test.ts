import { UserRepo } from '@woolies/recruitment/domains/user/user.repo';

jest.mock('mongodb');
jest.mock('@woolies/recruitment/domains/user/user.mapper', () => ({
  UserMapper: {
    toDomain: () => ({}),
  },
}));

describe('UserRepo', () => {
  let userRepo: UserRepo;
  let mongoDbMock: any;
  let findOneMock: any;

  beforeAll(() => {
    findOneMock = jest.fn();
    mongoDbMock = {
      collection: () => ({ findOne: findOneMock }),
    };
    userRepo = new UserRepo(mongoDbMock);
  });

  describe('findUser', () => {
    it('returns null if user is not found', async () => {
      (findOneMock as any).mockImplementation(() => null);

      const result = await userRepo.findTestUser();

      expect(result).toBeNull();
    });

    it('returns a User object if user is found', async () => {
      (findOneMock as any).mockImplementation(() => ({}));

      const result = await userRepo.findTestUser();
      expect(result).not.toBeNull();
    });
  });
});
