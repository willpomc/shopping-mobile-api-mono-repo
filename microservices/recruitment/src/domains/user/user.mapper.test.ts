import { UserMapper } from '@woolies/recruitment/domains/user/user.mapper';
import { User } from '@woolies/recruitment/domains/user/user';
import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';

describe('UserMapper', () => {
  const userRaw = {
    _id: 'abc',
    name: 'name',
    token: 'token',
  };

  const user = User.create({
    name: 'Roger',
    token: new UniqueEntityID('token'),
  });

  it('converts userRaw to domain User as expected', () => {
    const user = UserMapper.toDomain(userRaw);

    expect(user).toMatchSnapshot();
  });

  it('converts user to a format for the Db', () => {
    const userToPersist = UserMapper.toPersistence(user);

    expect(userToPersist).toMatchSnapshot({
      _id: expect.any(String),
    });
  });

  it('converts user to a format for the API consumer', () => {
    const userDTO = UserMapper.toDTO(user);

    expect(userDTO).toMatchSnapshot();
  });
});
