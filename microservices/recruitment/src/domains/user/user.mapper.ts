import { Mapper } from '@woolies/shared/domain/data/Mapper';
import { User, UserRaw, UserDTO } from '@woolies/recruitment/domains/user/user';
import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';

export class UserMapper extends Mapper {
  // Transform dumb user object to an instantiation of a User
  // so that logic pertaining to a user can be executed via this
  // co-located logic inside this instantiation.
  public static toDomain(userRaw: UserRaw): User {
    const { _id, name } = userRaw;
    return User.create(
      {
        name,
        token: new UniqueEntityID(_id),
      },
      new UniqueEntityID(_id)
    );
  }

  // Transform user to a Data Transfer Object to be sent back to client
  public static toDTO(user: User): UserDTO {
    return {
      name: user.props.name,
      token: user.props.token.toString(),
    };
  }

  // Transform user for saving to DB
  // Not used - did this to validate the design
  public static toPersistence(user: User): UserRaw {
    const { id } = user;
    const { name } = user.props;

    return {
      _id: id.toString(),
      name,
    };
  }
}
