// Repo designed around mongoDb.
// If we swap DBs, we just modify here.
import { Db } from 'mongodb';
import { Repo } from '@woolies/shared/domain/data/Repo';
import { User } from '@woolies/recruitment/domains/user/user';
import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';
import { UserMapper } from '@woolies/recruitment/domains/user/user.mapper';

export class UserRepo implements Repo<User> {
  private readonly mongoDb: Db;
  private readonly collectionName: string = 'user';

  constructor(mongoDb: Db) {
    this.mongoDb = mongoDb;
  }

  async exists(id: UniqueEntityID): Promise<boolean> {
    const result = this.mongoDb.collection(this.collectionName).find({ id: id.toString() });
    return !!result;
  }

  async save(user: User): Promise<User> {
    // Logic to persist user to DB after transformation.
    // But no requirement to do this, so let's stick to the requirements and don't bother
    // implementing.
    return user;
  }

  async findTestUser(): Promise<User | null> {
    const userRaw = await this.mongoDb.collection(this.collectionName).findOne({});

    if (!userRaw || false) {
      return null;
    }

    return UserMapper.toDomain(userRaw);
  }

  // Not used - did this to validate the design
  async saveUser(user: User, id: UniqueEntityID): Promise<User | null> {
    const { _id, ...userRawRest } = UserMapper.toPersistence(user);
    const {
      result: { ok },
    } = await this.mongoDb
      .collection(this.collectionName)
      .updateOne({ _id: id.toString() }, { $set: userRawRest }, { upsert: true });

    if (!ok) {
      return null;
    }

    return user;
  }
}
