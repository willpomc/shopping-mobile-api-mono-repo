import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';
import {AggregateRoot} from '@woolies/shared/domain/entity/AggregateRoot';

// UserRaw is the type of the data stored in the DB.
export interface UserRaw {
  _id: string;
  name: string;
}

// UserDTO is the type of the data passed back to the client
export interface UserDTO {
  name: string;
  token: string;

  // Could return subset of other values of the user, but that's not the API requirement,
  // (even though our collection has these values).
  // suburb: string
  // fullPhone: string
}

interface UserProps {
  name: string;
  token: UniqueEntityID;
}

export class User extends AggregateRoot<UserProps> {
  constructor(props: UserProps, id?: UniqueEntityID) {
    super(props, id);
  }

  // Useful to use a static create method (factory pattern) so we
  // can return something else other than a 'User'. Eg. we could
  // return something that encapsulates the User object with error logic (eg. Result<User>)
  // which may be needed if there is a problem in the creation
  // process (eg. missing/invalid prop).
  public static create(props: UserProps, id?: UniqueEntityID): User {
    // TODO: Sad path
    // If say name or token was missing, we could throw an error here.
    // But is that a bit heavy handed for a data issue?
    // Or we could return null (but then the consumer of this create function
    // has no idea why it failed just from a null object)...
    // Or we could return some Result object for errors, and also success
    // then the consumer could execute logic to know if there was an error
    // or it was a success.

    // Happy path
    return new User(props, id);
  }
}
