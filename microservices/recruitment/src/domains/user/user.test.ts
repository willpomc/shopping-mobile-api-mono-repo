import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';
import { User } from './user';

describe('User', () => {
  it('compares 2 instances of User objects with the same id and returns true', () => {
    const instance1 = User.create(
      {
        name: 'Bob',
        token: new UniqueEntityID(),
      },
      new UniqueEntityID('bobs-id')
    );

    const instance2 = User.create(
      {
        name: 'Bob',
        token: new UniqueEntityID(),
      },
      new UniqueEntityID('bobs-id')
    );

    // Ensure the 2 instances are different
    expect(instance1 === instance2).toBe(false);

    // Do the value comparison now
    expect(instance1.equals(instance2)).toBe(true);
  });
});
