import {initialiseServer} from '@woolies/shared/express/server';
import {getUseCaseRoutes} from './useCases';
import {getLogger} from '@woolies/shared/utils/logger';

const logger = getLogger('recruitment', module);

// Initialise server with routes specific to this microservice
logger.info('Initialising server now...');
initialiseServer(getUseCaseRoutes);
