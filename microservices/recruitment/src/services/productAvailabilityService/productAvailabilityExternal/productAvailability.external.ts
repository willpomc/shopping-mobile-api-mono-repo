import {ExternalService} from '@woolies/shared/domain/service/ExternalService';
import fetch from 'node-fetch';
import {ProductList} from '../productList/productList';
import {ProductListMapper} from '../productList/productList.mapper';

interface ProductAvailabilityCallProps {
  token: string
}

export class ProductAvailabilityExternalService extends ExternalService<ProductAvailabilityCallProps, ProductList> {
  constructor(serviceUrl: string) {
    super(serviceUrl);
  }

  public async call(callProps: ProductAvailabilityCallProps): Promise<ProductList> {
    const result = await fetch(`${this.serviceUrl}?token=${callProps.token}`);

    // This should throw if status is not OK
    ExternalService.validateResponseStatus(result);

    const resultJson = await result.json();

    return ProductListMapper.toDomain(resultJson);
  }
}
