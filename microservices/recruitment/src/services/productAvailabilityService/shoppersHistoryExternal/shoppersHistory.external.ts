import { ExternalService } from '@woolies/shared/domain/service/ExternalService';
import fetch from 'node-fetch';
import { ProductList } from '../productList/productList';
import { ProductDTO } from '../../../domains/product/product';
import { ShoppersHistoryMapper } from './shoppersHistory.mapper';
import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';

interface ShoppersHistoryCallProps {
  token: string;
}

export interface ShopperHistoryDTO {
  customerId: string;
  products: ProductDTO[];
}

export interface ShopperHistory {
  customerId: UniqueEntityID;
  productList: ProductList;
}

export class ShoppersHistoryExternalService extends ExternalService<ShoppersHistoryCallProps, ShopperHistory[]> {
  constructor(serviceUrl: string) {
    super(serviceUrl);
  }

  public async call(callProps: ShoppersHistoryCallProps): Promise<ShopperHistory[]> {
    const result = await fetch(`${this.serviceUrl}?token=${callProps.token}`);

    // This should throw if status is not OK
    ExternalService.validateResponseStatus(result);

    const resultJson = await result.json();

    return ShoppersHistoryMapper.toDomain(resultJson);
  }
}
