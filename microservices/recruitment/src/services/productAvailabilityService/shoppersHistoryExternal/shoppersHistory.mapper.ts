import { Mapper } from '@woolies/shared/domain/data/Mapper';
import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';
import { ShopperHistory, ShopperHistoryDTO } from './shoppersHistory.external';
import { ProductList } from '../productList/productList';
import { Product } from '../../../domains/product/product';
import PreciseNumber from 'big.js';

export class ShoppersHistoryMapper extends Mapper {
  public static toDomain(shoppersHistoryDTO: ShopperHistoryDTO[]): ShopperHistory[] {
    return shoppersHistoryDTO.map(shopperHistoryDTO => {
      const { customerId, products } = shopperHistoryDTO;

      return {
        customerId: new UniqueEntityID(customerId),
        productList: ProductList.create({
          products: products.map<Product>(({ name, price, quantity }) =>
            Product.create({
              name,
              price: PreciseNumber(price),
              quantity: PreciseNumber(quantity),
            })
          ),
        }),
      };
    });
  }
}
