import {ProductAvailabilityService} from '@woolies/recruitment/services/productAvailabilityService/productAvailabilityService';
import {ShoppersHistoryExternalService} from '@woolies/recruitment/services/productAvailabilityService/shoppersHistoryExternal/shoppersHistory.external';

jest.mock('@woolies/recruitment/services/productAvailabilityService/productAvailabilityExternal/productAvailability.external', () => {
  const {ProductListMapper} = require('@woolies/recruitment/services/productAvailabilityService/productList/productList.mapper');
  return {
  ProductAvailabilityExternalService: class ProductAvailabilityExternalService {
    public call() {
      return ProductListMapper.toDomain([
        {
          "name": "Test Product A",
          "price": 99.99,
          "quantity": 0
        },
        {
          "name": "Test Product B",
          "price": 101.99,
          "quantity": 0
        },
        {
          "name": "Test Product C",
          "price": 10.99,
          "quantity": 0
        },
        {
          "name": "Test Product D",
          "price": 5,
          "quantity": 0
        },
        {
          "name": "Test Product F",
          "price": 999999999999,
          "quantity": 0
        }
      ]);
    }
  }
}});

jest.mock('@woolies/recruitment/services/productAvailabilityService/shoppersHistoryExternal/shoppersHistory.external', () => {
  const {ShoppersHistoryMapper} = require('@woolies/recruitment/services/productAvailabilityService/shoppersHistoryExternal/shoppersHistory.mapper');

  return {
    ShoppersHistoryExternalService: class ShoppersHistoryExternalService {
      public call() {
        return ShoppersHistoryMapper.toDomain([
          {
            "customerId": '123',
            "products": [
              {
                "name": "Test Product A",
                "price": 99.99,
                "quantity": 3
              },
              {
                "name": "Test Product B",
                "price": 101.99,
                "quantity": 1
              },
              {
                "name": "Test Product F",
                "price": 999999999999,
                "quantity": 1
              }
            ]
          },
          {
            "customerId": '23',
            "products": [
              {
                "name": "Test Product A",
                "price": 99.99,
                "quantity": 2
              },
              {
                "name": "Test Product B",
                "price": 101.99,
                "quantity": 3
              },
              {
                "name": "Test Product F",
                "price": 999999999999,
                "quantity": 1
              }
            ]
          },
          {
            "customerId": '23',
            "products": [
              {
                "name": "Test Product C",
                "price": 10.99,
                "quantity": 2
              },
              {
                "name": "Test Product F",
                "price": 999999999999,
                "quantity": 2
              }
            ]
          },
          {
            "customerId": '23',
            "products": [
              {
                "name": "Test Product A",
                "price": 99.99,
                "quantity": 1
              },
              {
                "name": "Test Product B",
                "price": 101.99,
                "quantity": 1
              },
              {
                "name": "Test Product C",
                "price": 10.99,
                "quantity": 1
              }
            ]
          }
        ])
      }
    }
  }});

describe('ProductAvailabilityService', () => {
  let productAvailabilityService: ProductAvailabilityService;

  beforeEach(() => {
    productAvailabilityService = new ProductAvailabilityService();
  });

  it('sorts Low',  async () => {
    const result = await productAvailabilityService.execute({sortOption: 'Low'});
    expect(result).toMatchSnapshot();
  });

  it('sorts High',  async () => {
    const result = await productAvailabilityService.execute({sortOption: 'High'});
    expect(result).toMatchSnapshot();
  });

  it('sorts Ascending',  async () => {
    const result = await productAvailabilityService.execute({sortOption: 'Ascending'});
    expect(result).toMatchSnapshot();
  });

  it('sorts Descending',  async () => {
    const result = await productAvailabilityService.execute({sortOption: 'Descending'});
    expect(result).toMatchSnapshot();
  });

  it('sorts Recommended',  async () => {
    const result = await productAvailabilityService.execute({sortOption: 'Recommended'});
    expect(result).toMatchSnapshot();
  });
});
