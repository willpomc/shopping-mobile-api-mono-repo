import { DomainService } from '@woolies/shared/domain/service/DomainService';
import { ProductAvailabilityExternalService } from '@woolies/recruitment/services/productAvailabilityService/productAvailabilityExternal/productAvailability.external';
import {
  ShopperHistory,
  ShoppersHistoryExternalService,
} from '@woolies/recruitment/services/productAvailabilityService/shoppersHistoryExternal/shoppersHistory.external';
import { ProductList } from '@woolies/recruitment/services/productAvailabilityService/productList/productList';
import { getLogger } from '@woolies/shared/utils/logger';
import { Product } from '@woolies/recruitment/domains/product/product';
import config from 'config';
import PreciseNumber from 'big.js';

const logger = getLogger('recruitment', module);

interface ServiceParams {
  sortOption: 'Low' | 'High' | 'Ascending' | 'Descending' | 'Recommended';
}

export class ProductAvailabilityService extends DomainService<undefined, ServiceParams, ProductList> {
  private readonly productAvailabilityExternalService: ProductAvailabilityExternalService;
  private readonly shopperHistoryExternalService: ShoppersHistoryExternalService;

  constructor() {
    super();

    this.productAvailabilityExternalService = new ProductAvailabilityExternalService(config.get('availableProductUrl'));
    this.shopperHistoryExternalService = new ShoppersHistoryExternalService(config.get('shopperHistoryUrl'));
  }

  public async execute(serviceParams: ServiceParams): Promise<ProductList> {
    const availableProducts = await this.productAvailabilityExternalService.call({
      token: process.env.API_TOKEN as string,
    });
    const shoppersHistory = await this.shopperHistoryExternalService.call({ token: process.env.API_TOKEN as string });

    const { sortOption } = serviceParams;

    let sortedAvailableProducts;
    switch (sortOption) {
      case 'Low': {
        logger.info('Low sort');
        sortedAvailableProducts = availableProducts.sortByPrice('ascending');
        break;
      }

      case 'High': {
        logger.info('High sort');
        sortedAvailableProducts = availableProducts.sortByPrice('descending');
        break;
      }

      case 'Ascending': {
        logger.info('Ascending sort');
        sortedAvailableProducts = availableProducts.sortByName('ascending');
        break;
      }

      case 'Descending': {
        logger.info('Descending sort');
        sortedAvailableProducts = availableProducts.sortByName('descending');
        break;
      }

      case 'Recommended': {
        logger.info('Recommended sort');
        const historyProductQuantityTotalsMap: { [key: string]: PreciseNumber } = {};

        // Aggregate the quantities for each product into historyProductQuantityTotalsMap
        shoppersHistory.forEach((shopperHistory: ShopperHistory) => {
          shopperHistory.productList.props.products.forEach((product: Product) => {
            const { name, quantity } = product.props;

            historyProductQuantityTotalsMap[name] = PreciseNumber(historyProductQuantityTotalsMap[name] || 0).plus(quantity);
          });
        });

        // Create a product list augmented with aggregateQuantity/popularityIndex
        const productsAugmented: Product[] = availableProducts.props.products.map((product: Product) => {
          const { name } = product.props;

          const aggregateQuantity = historyProductQuantityTotalsMap[name] || PreciseNumber(0);

          return Product.create({
            ...product.props,
            popularityIndex: parseFloat(aggregateQuantity.toString()),
          });
        });

        const productsListAugmented = ProductList.create({ products: productsAugmented });

        sortedAvailableProducts = productsListAugmented.sortByPopularity();
        break;
      }

      default: {
        sortedAvailableProducts = availableProducts;
      }
    }
    return sortedAvailableProducts;
  }
}
