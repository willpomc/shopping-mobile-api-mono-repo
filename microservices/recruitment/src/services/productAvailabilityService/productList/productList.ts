import { ValueObject } from '@woolies/shared/domain/valueObject/ValueObject';
import { Product, ProductDTO, ProductRaw } from '../../../domains/product/product';

const ascending = 'ascending';
const descending = 'descending';
type SortDirection = 'ascending' | 'descending';

interface ProductListProps {
  products: Product[];
}

export type ProductListRaw = ProductRaw[];
export type ProductListDTO = ProductDTO[];

export class ProductList extends ValueObject<ProductListProps> {
  constructor(props: ProductListProps) {
    super(props);
  }

  public static create(props: ProductListProps): ProductList {
    return new ProductList(props);
  }

  private sort(direction: SortDirection, valueA: string | number, valueB: string | number): number {
    if (valueA < valueB) {
      return direction === ascending ? -1 : 1;
    } else if (valueA > valueB) {
      return direction === ascending ? 1 : -1;
    }

    return 0;
  }

  public sortByName(direction: SortDirection): ProductList {
    // Sort then return
    return ProductList.create({
      products: [...this.props.products].sort((a, b) => this.sort(direction, a.props.name, b.props.name)),
    });
  }

  public sortByPrice(direction: SortDirection): ProductList {
    return ProductList.create({
      products: [...this.props.products].sort((a, b) =>
        this.sort(direction, parseFloat(a.props.price.toString()), parseFloat(b.props.price.toString()))
      ),
    });
  }

  public sortByQuantity(direction: SortDirection): ProductList {
    // Sort then return
    return ProductList.create({
      products: [...this.props.products].sort((a, b) =>
        this.sort(direction, parseFloat(a.props.quantity.toString()), parseFloat(b.props.quantity.toString()))
      ),
    });
  }

  public sortByPopularity(): ProductList {
    // Sort then return
    return ProductList.create({
      products: [...this.props.products].sort((a, b) =>
        this.sort(descending, a.props.popularityIndex || 0, b.props.popularityIndex || 0)
      ),
    });
  }
}
