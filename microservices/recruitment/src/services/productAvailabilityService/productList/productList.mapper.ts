import { Mapper } from '@woolies/shared/domain/data/Mapper';
import { ProductList, ProductListDTO, ProductListRaw } from './productList';
import { ProductMapper } from '@woolies/recruitment/domains/product/product.mapper';

export class ProductListMapper extends Mapper {
  public static toDomain(productListRaw: ProductListRaw): ProductList {
    return ProductList.create({
      products: productListRaw.map(productRaw => {
        return ProductMapper.toDomain(productRaw);
      }),
    });
  }

  // Note: we filter out the popularityIndex of a Product
  public static toDTO(productList: ProductList): ProductListDTO {
    return productList.props.products.map(product => ProductMapper.toDTO(product));
  }
}
