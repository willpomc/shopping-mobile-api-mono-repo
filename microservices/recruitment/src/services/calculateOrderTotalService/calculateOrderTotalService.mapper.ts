import { Mapper } from '@woolies/shared/domain/data/Mapper';
import PreciseNumber from 'big.js';
import {
  ServiceParams,
  ServiceParamsRaw,
} from '@woolies/recruitment/services/calculateOrderTotalService/calculateOrderTotalService';

export class CalculateOrderTotalServiceMapper extends Mapper {
  public static requestToDomain(serviceParamsRaw: ServiceParamsRaw): ServiceParams {
    const { Products, Quantities, Specials } = serviceParamsRaw;

    return {
      products: Products.map(({ Name, Price }) => ({
        name: Name,
        price: PreciseNumber(Price),
      })),
      quantities: Quantities.map(({ Name, Quantity }) => ({
        name: Name,
        quantity: PreciseNumber(Quantity),
      })),
      specials: Specials.map(({ Quantities, Total }) => ({
        quantities: Quantities.map(({ Name, Quantity }) => ({
          name: Name,
          quantity: PreciseNumber(Quantity),
        })),
        total: PreciseNumber(Total),
      })),
    };
  }
}
