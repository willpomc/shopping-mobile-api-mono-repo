import PreciseNumber from 'big.js';
import { DomainService } from '@woolies/shared/domain/service/DomainService';
import { TotalRequest } from '@woolies/recruitment/useCases/getTrolleyTotalUseCase/getTrolleyTotalUseCase';
import { getLogger } from '@woolies/shared/utils/logger';
import { Product } from '@woolies/recruitment/domains/product/product';
import { ProductMapper } from '@woolies/recruitment/domains/product/product.mapper';
import { CalculateOrderTotalServiceMapper } from '@woolies/recruitment/services/calculateOrderTotalService/calculateOrderTotalService.mapper';

const logger = getLogger('recruitment', module);

export type ServiceParamsRaw = TotalRequest;
export interface ServiceParams {
  products: {
    name: string;
    price: PreciseNumber;
  }[];
  quantities: {
    name: string;
    quantity: PreciseNumber;
  }[];
  specials: {
    quantities: {
      name: string;
      quantity: PreciseNumber;
    }[];
    total: PreciseNumber;
  }[];
}

export class CalculateOrderTotalService extends DomainService<{}, ServiceParamsRaw, string> {
  public execute(serviceParamsRaw: ServiceParamsRaw): string {
    const { products, quantities, specials: bundleSpecials } = CalculateOrderTotalServiceMapper.requestToDomain(
      serviceParamsRaw
    );

    const productsMap: { [key: string]: Product } = {};
    quantities.forEach(product => {
      const { name, quantity } = product;

      if (productsMap[name] === undefined) {
        productsMap[name] = ProductMapper.toDomain({
          name,
          quantity: 0,
          price: 0,
        });
      }

      const previousQuantity = productsMap[name].props.quantity;

      productsMap[name] = Product.create({
        ...productsMap[name].props,
        quantity: previousQuantity.plus(quantity),
      });
    });

    let currentLowestTotal = PreciseNumber(0);
    products.forEach(product => {
      const { name, price } = product;

      productsMap[name] = Product.create({
        ...productsMap[name].props,
        price: price,
      });
      currentLowestTotal = currentLowestTotal.plus(price.times(productsMap[name].props.quantity)); // currentLowestTotal += price * productsMap[name].quantity;

      logger.info('current currentLowestTotal', currentLowestTotal.toString());
    });

    bundleSpecials.forEach((bundleSpecial, bundleIndex) => {
      const { quantities, total: bundlePrice } = bundleSpecial;

      let bundleTotal = PreciseNumber(0);
      let bundleStillApplies = true;
      quantities.forEach(({ name, quantity: quantityAllowed }) => {
        if (bundleStillApplies) {
          const { price: originalUnitPrice, quantity: desiredQuantity } = productsMap[name].props;

          if (desiredQuantity.lt(quantityAllowed)) {
            bundleStillApplies = false;
          } else {
            const originalPriceQuantity = desiredQuantity.minus(quantityAllowed);
            bundleTotal = bundleTotal.plus(originalPriceQuantity.times(originalUnitPrice));
          }
        }
      });

      if (bundleStillApplies) {
        bundleTotal = bundleTotal.plus(bundlePrice);

        if (bundleTotal.lt(currentLowestTotal)) {
          currentLowestTotal = bundleTotal;
          logger.info(
            `Bundle number ${bundleIndex} is now the best with the lowest total of $${currentLowestTotal.toString()}`
          );
        }
      }
    });

    logger.info('currentLowestTotal', currentLowestTotal);

    return currentLowestTotal.toString();
  }
}
