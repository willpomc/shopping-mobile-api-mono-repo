import { CalculateOrderTotalService } from '@woolies/recruitment/services/calculateOrderTotalService/calculateOrderTotalService';
import { ProductAvailabilityService } from '@woolies/recruitment/services/productAvailabilityService/productAvailabilityService';

describe('CalculateOrderTotalService', () => {
  let calculateOrderTotalService: CalculateOrderTotalService;

  beforeEach(() => {
    calculateOrderTotalService = new CalculateOrderTotalService();
  });

  it('calculates the lowest total with sample input A', () => {
    const total = calculateOrderTotalService.execute({
      Products: [
        {
          Name: 'Product 0',
          Price: 1.44752655711375,
        },
        {
          Name: 'Product 1',
          Price: 9.04295306142557,
        },
        {
          Name: 'Product 2',
          Price: 11.5656574287292,
        },
      ],
      Specials: [
        {
          Quantities: [
            {
              Name: 'Product 0',
              Quantity: 4,
            },
            {
              Name: 'Product 1',
              Quantity: 3,
            },
            {
              Name: 'Product 2',
              Quantity: 2,
            },
          ],
          Total: 9.69269471063542,
        },
      ],
      Quantities: [
        {
          Name: 'Product 0',
          Quantity: 5,
        },
        {
          Name: 'Product 1',
          Quantity: 3,
        },
        {
          Name: 'Product 2',
          Quantity: 2,
        },
      ],
    });

    expect(total).toEqual('11.14022126774917');
  });

  it('calculates the lowest total with sample input B', () => {
    const total = calculateOrderTotalService.execute({
      Products: [
        {
          Name: 'Product 0',
          Price: 7.21998603652231,
        },
        {
          Name: 'Product 1',
          Price: 12.1010051072114,
        },
        {
          Name: 'Product 2',
          Price: 8.75069418863891,
        },
      ],
      Specials: [],
      Quantities: [
        {
          Name: 'Product 0',
          Quantity: 6,
        },
        {
          Name: 'Product 1',
          Quantity: 2,
        },
        {
          Name: 'Product 2',
          Quantity: 7,
        },
      ],
    });

    expect(total).toEqual('128.77678575402903');
  });
});
