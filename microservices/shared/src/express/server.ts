import http, { RequestListener } from 'http';
import { getApp } from './app';
import { getLogger } from '@woolies/shared/utils/logger';

const logger = getLogger('shared', module);

export const initialiseServer = (useCaseRoutesGetter: UseCaseRoutesGetter): Promise<void> =>
  getApp(useCaseRoutesGetter)
    .then((app: RequestListener) => {
      const { PORT = 3000 } = process.env;
      const server = http.createServer(app);

      server.listen(PORT, () => logger.info(`Server is running http://localhost:${PORT}...`));
    })
    .catch(err => {
      logger.error(`Server failed to start up (${err})`);
    });
