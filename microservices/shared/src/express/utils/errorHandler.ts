import { Response, NextFunction } from 'express';
import { HTTPClientError, HTTP404Error } from './httpErrors';
import {getLogger} from '@woolies/shared/utils/logger';

const logger = getLogger('shared', module);

export const notFoundError = (): void => {
  throw new HTTP404Error('Method not found.');
};

export const clientError = (err: Error, res: Response, next: NextFunction): void => {
  if (err instanceof HTTPClientError) {
    logger.warn(err);
    res.status(err.statusCode).send(err.message);
  } else {
    next(err);
  }
};

// No 'next' handler provided - this is the END OF THE LINE!
// This should catch all UNKNOWN errors.
// There is still of cause 'uncaughtException' and 'unhandledRejection'
// but if it goes there, we've not done too well :O
export const serverError = (err: Error, res: Response): void => {
  logger.error(err);

  if (process.env.NODE_ENV === 'production') {
    res.status(500).send('Internal Server Error');
  } else {
    res.status(500).send(err.stack);
  }
};
