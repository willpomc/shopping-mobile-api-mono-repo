import { Router } from 'express';

type Wrapper = (router: Router) => void;

export const applyMiddleware = (middlewareWrappers: Wrapper[], router: Router): void => {
  for (const wrapper of middlewareWrappers) {
    wrapper(router);
  }
};

export const applyRoutes = (routes: Route[], router: Router): void => {
  for (const route of routes) {
    const { path, method, handler } = route;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (router as any)[method](path, handler);
  }
};
