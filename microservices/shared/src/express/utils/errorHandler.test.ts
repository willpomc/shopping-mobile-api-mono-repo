import { notFoundError, clientError, serverError } from '@woolies/shared/express/utils/errorHandler';
import { HTTP400Error, HTTP404Error } from '@woolies/shared/express/utils/httpErrors';
import { Response } from 'express';

describe('ErrorHandler', () => {
  let sendMock: jest.Mock;
  let statusMock: jest.Mock;
  let resMock: any;

  beforeAll(() => {
    sendMock = jest.fn();
    statusMock = jest.fn().mockImplementation(() => ({ send: sendMock }));
    resMock = {
      status: statusMock,
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('notFoundError handler should throw a 404 Error', () => {
    expect(notFoundError).toThrow(new HTTP404Error('Method not found.'));
  });

  describe('clientError', () => {
    it('clientError handler should set status and message for a Client Error', () => {
      const newClientError = new HTTP400Error('bugger');
      clientError(newClientError, resMock, () => {});

      expect(statusMock).toBeCalledWith(newClientError.statusCode);
      expect(sendMock).toBeCalledWith('bugger');
    });

    it('clientError handler should set status and message for a Client Error', () => {
      const newError = new Error('not a client error');
      const nextFn: jest.Mock = jest.fn();
      clientError(newError, resMock, nextFn);

      expect(statusMock).not.toBeCalled();
      expect(sendMock).not.toBeCalled();
      expect(nextFn).toBeCalledWith(newError);
    });
  });

  describe('serverError', () => {
    const OLD_ENV = process.env;

    beforeEach(() => {
      process.env = { ...OLD_ENV };
      delete process.env.NODE_ENV;
    });

    afterEach(() => {
      process.env = OLD_ENV;
    });

    it('serverError handler should set status and message appropriately for Production', () => {
      process.env.NODE_ENV = 'production';
      const newError = new Error('server error');
      serverError(newError, resMock);

      expect(statusMock).toBeCalledWith(500);
      expect(sendMock).toBeCalledWith('Internal Server Error');
    });

    it('serverError handler should set status and message appropriately for non production', () => {
      const newError = new Error('server error');
      newError.stack = 'Booyah';
      serverError(newError, resMock);

      expect(statusMock).toBeCalledWith(500);
      expect(sendMock).toBeCalledWith('Booyah');
    });
  });
});
