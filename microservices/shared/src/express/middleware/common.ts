import { Router } from 'express';
import cors from 'cors';
import parser from 'body-parser';
import compression from 'compression';

export const handleCors = (router: Router): void => {
  router.use(cors({ credentials: true, origin: true }));
};

export const handleBodyRequestParsing = (router: Router): void => {
  router.use(parser.urlencoded({ extended: true }));
  router.use(parser.json());
};

export const handleCompression = (router: Router): void => {
  router.use(compression());
};

export const handleFaviconRequest = (router: Router): void => {
  router.get('/favicon.ico', (req, res) => res.sendStatus(204));
};
