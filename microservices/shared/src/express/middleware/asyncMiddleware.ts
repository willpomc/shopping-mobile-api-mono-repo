import { Request, Response, RequestHandler, NextFunction } from 'express';

// If the async 'middleware' throws an error, we direct it to our
// error middleware which centralises the error handling.
export const asyncMiddleware = (middleware: RequestHandler): RequestHandler => (
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => Promise.resolve(middleware(req, res, next)).catch(next);
