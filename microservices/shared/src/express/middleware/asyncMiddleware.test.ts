import { asyncMiddleware } from '@woolies/shared/express/middleware/asyncMiddleware';

describe('asyncMiddleware', () => {
  it('resolves the async middleware ok', async () => {
    const middleware = async () => {
      return;
    };

    const wrappedAsyncMiddleware = asyncMiddleware(middleware);

    const req: any = {};
    const res: any = {};
    const nextFn: jest.Mock = jest.fn();

    await wrappedAsyncMiddleware(req, res, nextFn);

    expect(nextFn).not.toBeCalled();
  });

  it('calls the next error handler middleware when there is an error thrown', async () => {
    const error = new Error('boom');
    const middleware = async () => {
      throw error;
    };

    const wrappedAsyncMiddleware = asyncMiddleware(middleware);

    const req: any = {};
    const res: any = {};
    const nextFn: jest.Mock = jest.fn();

    await wrappedAsyncMiddleware(req, res, nextFn);

    expect(nextFn).toBeCalledWith(error);
  });
});
