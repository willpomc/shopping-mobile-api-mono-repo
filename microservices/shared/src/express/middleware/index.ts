import { handleFaviconRequest, handleCors, handleBodyRequestParsing, handleCompression } from './common';

export default [handleFaviconRequest, handleCors, handleBodyRequestParsing, handleCompression];
