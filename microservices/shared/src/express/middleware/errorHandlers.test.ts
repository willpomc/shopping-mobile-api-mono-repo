import errorHandlers from './errorHandlers';

jest.mock('../utils/errorHandler');

describe('errorHandlers', () => {
  it('all handlers call router.use', () => {
    const router: any = {
      use: jest.fn(),
    };
    errorHandlers.forEach(handler => handler(router));

    expect(router.use.mock.calls.length).toEqual(errorHandlers.length);
  });
});
