import express from 'express';
import { applyMiddleware, applyRoutes } from './utils';
import middleware from './middleware';
import errorHandlers from './middleware/errorHandlers';
import { getMongoDb } from './model/mongo';
import { RequestListener } from 'http';
import { getLogger } from '@woolies/shared/utils/logger';

const logger = getLogger('shared', module);

export const getApp = async (getUseCaseRoutes: UseCaseRoutesGetter): Promise<RequestListener> => {
  process.on('uncaughtException', e => {
    logger.error(`uncaughtException: ${e}`);
    process.exit(1);
  });

  process.on('unhandledRejection', e => {
    logger.error(`unhandledRejection: ${e}`);
    process.exit(1);
  });

  const app = express();
  const router = express.Router();
  const mongoDb = await getMongoDb();
  const routes = getUseCaseRoutes(mongoDb);
  applyMiddleware(middleware, router);
  applyRoutes(([] as Route[]).concat(...routes), router);
  applyMiddleware(errorHandlers, router);

  // Set baseUrl
  app.use('/api', router);

  return app;
};
