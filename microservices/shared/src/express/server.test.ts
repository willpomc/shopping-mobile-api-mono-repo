import http from 'http';
import { initialiseServer } from '@woolies/shared/express/server';
import { getApp } from './app';

jest.mock('./app');

/* For some reason, I am getting 'TypeError: Cannot read property 'prototype' of undefined'
when I do below:
jest.mock('http', () => ({
  createServer: jest.fn(),
}));

Monkey patching instead
*/

http.createServer = jest.fn();

describe('initialiseServer', () => {
  it('calls listen successfully', async () => {
    (getApp as any).mockImplementation(async () => ({}));
    const listenMock = jest.fn();
    (http.createServer as any).mockImplementation(() => ({ listen: listenMock }));
    const useCaseRoutesGetter: any = () => {};

    await initialiseServer(useCaseRoutesGetter);

    expect(listenMock).toBeCalled();
  });
});
