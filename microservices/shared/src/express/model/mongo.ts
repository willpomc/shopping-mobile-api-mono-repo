// Instantiation of stores/dbs/queues can go here in one place.
// The only layer that depends on this is/should be the infrastructure/repository layer.
import { Db, MongoClient } from 'mongodb';
import config from 'config';
import dotEnv from 'dotenv';
import findConfig from 'find-config';
import { getLogger } from '@woolies/shared/utils/logger';

dotEnv.config({ path: findConfig('.env') });

const logger = getLogger('shared', module);
let mongoClient: MongoClient;
let mongoDb: Db;

export const getMongoDb = async (): Promise<Db> => {
  if (!mongoDb) {
    const mongoUrl = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${config.get('mongoCluster')}`;
    mongoClient = new MongoClient(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });
    await mongoClient.connect();
    logger.info('Connected correctly to DB');

    mongoDb = mongoClient.db(config.get('mongoDatabase'));
  }

  return mongoDb;
};

export const destroyMongoDb = async(): Promise<void> => {
  await mongoClient.close();
};
