import { getMongoDb } from '@woolies/shared/express/model/mongo';
import { Db, MongoClient } from 'mongodb';

jest.mock('mongodb', () => ({
  MongoClient: jest.fn(),
}));

describe('getMongoDb', () => {
  let connectMock: any;
  let dbMock: any;

  beforeEach(() => {
    connectMock = jest.fn();
    dbMock = jest.fn().mockImplementation(() => ({}));
    (MongoClient as any).mockImplementation(() => ({
      connect: connectMock,
      db: dbMock,
    }));
  });

  afterEach(() => {
    jest.clearAllMocks();
    jest.resetModules();
  });

  it('it should call mongoClient connect/db on the first call', async () => {
    expect(await getMongoDb()).toEqual({});
    expect(connectMock).toBeCalled();
    expect(dbMock).toBeCalledWith('mongoDatabase');
  });

  it('it should not call mongoClient connect/db on the subsequent calls', async () => {
    expect(await getMongoDb()).toEqual({});
    jest.clearAllMocks();
    expect(await getMongoDb()).toEqual({});
    expect(connectMock).not.toBeCalled();
    expect(dbMock).not.toBeCalled();
  });
});
