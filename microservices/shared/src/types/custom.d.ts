type Handler = (req: import('express').Request, res: import('express').Response, next: import('express').NextFunction) => void | Promise<void>;

type Route = {
  path: string;
  method: string;
  handler: Handler | Handler[];
};

interface UseCaseRoutesGetter {
  (db: import('mongodb').Db): Route[];
}
