import { isEqual } from './isEqual';

describe('isEqual', () => {
  it('should return true when objects are equal', () => {
    expect(isEqual({ a: 123, h: 991 }, { a: 123, h: 991 })).toBe(true);
  });

  it('should return false when objects are not equal', () => {
    expect(isEqual({ a: 123, h: 991 }, { a: 5123, h: 931 })).toBe(false);
  });
});
