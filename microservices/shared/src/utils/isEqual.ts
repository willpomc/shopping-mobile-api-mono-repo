interface ObjectToCompare {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}

export const isEqual = (obj1: ObjectToCompare, obj2: ObjectToCompare): boolean =>
  Object.keys(obj1).length === Object.keys(obj2).length && Object.keys(obj1).every(key => obj1[key] === obj2[key]);
