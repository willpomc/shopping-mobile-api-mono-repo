import bunyan from 'bunyan';
import * as Logger from 'bunyan';

const loggerMap: {[key: string]: Logger} = {};

export const getLogger = (loggerName: string, module: NodeModule): Logger => {
  if (!loggerMap[loggerName]) {
    loggerMap[loggerName] = bunyan.createLogger({name: loggerName});
  }

  const logger = loggerMap[loggerName];

  if (module) {
    const file = module.filename;
    const fileParts = file.split('/');
    const truncatedFilename = fileParts[fileParts.length - 1];

    return logger.child({source: truncatedFilename});
  }

  return logger;
};
