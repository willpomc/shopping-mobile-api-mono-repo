import { Identifier } from '@woolies/shared/domain/id/identifier';

describe('identifier', () => {
  const identifier = new Identifier<string>('abc');

  it('equals returns true when id matches', () => {
    const anotherIdentifier = new Identifier<string>('abc');
    expect(identifier.equals(anotherIdentifier)).toBe(true);
  });

  it('equals returns false when id does not match', () => {
    const anotherIdentifier = new Identifier<string>('abc1');
    expect(identifier.equals(anotherIdentifier)).toBe(false);
  });
});
