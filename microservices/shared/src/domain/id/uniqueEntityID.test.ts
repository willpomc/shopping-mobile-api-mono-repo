import { UniqueEntityID } from '@woolies/shared/domain/id/uniqueEntityID';

describe('UniqueEntityID', () => {
  it('creates an instance with the supplied ID', () => {
    const newEntityId = new UniqueEntityID('123');

    expect(newEntityId.valueOf()).toEqual({ value: '123' });
  });

  it('creates an instance with a UUID if no ID is supplied', () => {
    const newEntityId = new UniqueEntityID();

    expect(newEntityId.valueOf()).toEqual({ value: expect.any(String) });
  });
});
