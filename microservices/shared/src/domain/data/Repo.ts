import { UniqueEntityID } from '../id/uniqueEntityID';

export interface Repo<T> {
  exists: (id: UniqueEntityID) => Promise<boolean>;
  save: (t: T) => Promise<T>;
}
