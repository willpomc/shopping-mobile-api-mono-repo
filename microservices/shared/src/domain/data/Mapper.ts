/* eslint-disable @typescript-eslint/no-unused-vars,@typescript-eslint/no-explicit-any */
export abstract class Mapper {
  public static toDomain(raw: any): any {
    throw new Error(`This static function should be overwritten by a subclass' definition`);
  }

  public static toDTO(domainObject: any): any {
    throw new Error(`This static function should be overwritten by a subclass' definition`);
  }

  public static toPersistence(domainObject: any): any {
    throw new Error(`This static function should be overwritten by a subclass' definition`);
  }
}

/* eslint-enable @typescript-eslint/no-unused-vars,@typescript-eslint/no-explicit-any */
