import { ValueObject } from './ValueObject';

describe('ValueObject', () => {
  it('compares 2 instances of value objects with the same values and returns true', () => {
    interface SomePropsInterface {
      prop: string;
    }

    class ValueObjectClass extends ValueObject<SomePropsInterface> {
      constructor(props: SomePropsInterface) {
        super(props);
      }
    }

    const instance1 = new ValueObjectClass({ prop: 'hi' });
    const instance2 = new ValueObjectClass({ prop: 'hi' });

    // Ensure the 2 instances are different
    expect(instance1 === instance2).toBe(false);

    // Do the value comparison now
    expect(instance1.equals(instance2)).toBe(true);
  });
});
