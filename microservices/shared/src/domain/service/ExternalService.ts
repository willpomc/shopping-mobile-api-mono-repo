import {Response} from 'node-fetch';
import {HTTP400Error} from '@woolies/shared/express/utils/httpErrors';

export abstract class ExternalService<CallProps, CallResult> {
  protected readonly serviceUrl: string;

  constructor(serviceUrl: string) {
    this.serviceUrl = serviceUrl;
  }

  public abstract call(callProps: CallProps): Promise<CallResult> | CallResult;

  protected static validateResponseStatus(fetchResponse: Response): boolean {
    if (fetchResponse.ok) { // res.status >= 200 && res.status < 300
      return true;
    } else {
      throw new HTTP400Error(fetchResponse.statusText);
    }
  }
}
