export abstract class DomainService<ServiceProps, ServiceParams, ServiceResult> {
  public readonly props?: ServiceProps;

  constructor(props?: ServiceProps) {
    this.props = props;
  }

  public abstract execute(serviceParams: ServiceParams): Promise<ServiceResult> | ServiceResult;
}
