const fs = require('fs');
const jestConfigBase = require('./tests/jest.config.base');

const microservicesDirPath = 'microservices/';
const microservicesToAlias = fs.readdirSync(microservicesDirPath).join('|');

module.exports = {
  ...jestConfigBase,

  testRegex: '(tests)/.+((\\.|/)(test))\\.[t]s$',

  // A map from regular expressions to module names that allow to stub out resources with a single module
  // moduleNameMapper: {},
  moduleNameMapper: {
    'shared\/src\/utils\/logger': '<rootDir>/tests/mocks/logger.js',

    // Need this to correctly map alias paths correctly
    // IMPORTANT: make sure this mapping goes last as it acts as a catch all as well.
    // TODO: for some reason ts-jest is not finding the babelConfig (can fix later)
    [`.+(${microservicesToAlias})/src/(.*)`]: '<rootDir>/microservices/$1/src/$2',
  },
};
