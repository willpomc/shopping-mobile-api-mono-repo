# Shopping Mobile API - Mono Repo

## Instructions For Running Locally
1. Set up the `.env` file with `DB_USER`, `DB_PASSWORD`, and `API_TOKEN`
2. Run `yarn`

### Dev
1. Run `yarn dev @woolies/recruitment` for Dev

### Non Dev
1. Run `yarn build @woolies/recruitment`
2. Run `yarn start @woolies/recruitment`

### Tests

#### Unit Tests
1. Run `yarn test`

#### Integration Tests
1. Run `yarn test:integration`

# Architecture / Build Notes
1. NodeJS / Express
2. Jest
3. Supertest
4. Lerna / Mono Repo
   - Can add more microservices to this repo
   - the `shared` module contains types/interfaces/classes
   - changes to shared module only will not trigger a re-build
   - changes to other modules (ie. the actual microservices) will
     trigger a build together with the shared module
5. Docker
6. Mongo Atlas
7. GCP - Cloud Build / Cloud Run / Container Registry
   - note: container needs time to warm up
8. BitBucket
9. Domain Driven Design (DDD) App Architecture 
